package at.yanbot.slick.asteroids;

import java.util.List;
import java.util.Random;

import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

public class Object {
	Shape shape;
	float xPos;
	float yPos;
	float xSpeed;
	float ySpeed;
	float rotation;
	float orientation;

	Transform translate;
	Transform rotate;
	
	public float getxPos() {
		return xPos;
	}

	public float getyPos() {
		return yPos;
	}

	Random rand = new Random();

	public void MoveDynamic(int delta) {
		translate = Transform.createTranslateTransform(xSpeed * delta * 0.5f, ySpeed * delta * 0.5f);
		this.shape = this.shape.transform(translate);
	}

	public void MoveStatic() {
		this.shape = this.shape.transform(translate);
	}

	public void RotateDynamic(int delta) {
		rotate = Transform.createRotateTransform(rotation * delta * 0.5f, this.shape.getCenterX(), this.shape.getCenterY());
		this.shape = this.shape.transform(rotate);
	}

	public void RotateStatic() {
		this.shape = this.shape.transform(rotate);
	}

	public Object(Shape shape, float rotation, float xSpeed, float ySpeed) {
		this.shape = shape;
		this.rotation = rotation;
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
	}

	protected Object() {
	}

	public Shape getShape() {
		return this.shape;
	}
	
	public void LoopBoundary(int windowWidth, int windowHeight)
	{
		float centerX = this.shape.getCenterX();
		float centerY = this.shape.getCenterY();
		
		float xMove = 0;
		float yMove = 0;
		if(centerX > windowWidth) xMove -= windowWidth;
		if(centerX < 0) xMove += windowWidth;
		if(centerY > windowHeight) yMove -= windowHeight;
		if(centerY < 0) yMove += windowHeight;
		
		Transform temp = Transform.createTranslateTransform(xMove, yMove);
		this.shape = this.shape.transform(temp);
	}

}
