package at.yanbot.slick.asteroids;

import org.newdawn.slick.geom.Polygon;
import at.yanbot.slick.asteroids.Projectile;

public class Ship extends Object {
	public Ship() {
		super();

		float[] points = { 0, -10, 5, 5, 0, 0, -5, 5 };
		this.shape = new Polygon(points);
		
		this.xSpeed = 0.05f;
		this.ySpeed = 0.05f;
	}
	
	public void setRotation(float value)
	{
		this.rotation = value;
	}
	
	public void Thrust()
	{
		float orientation = (float) (180 * Math.atan2(this.shape.getPoint(0)[0]-this.shape.getPoint(2)[0], this.shape.getPoint(0)[1]-this.shape.getPoint(2)[1]) / Math.PI);
		
        float xFactor = (float)Math.sin(Math.PI * (orientation / 180.0));
        float yFactor = (float)Math.cos(Math.PI * (orientation / 180.0));
        
        ySpeed += 0.0005f * yFactor;
        xSpeed += 0.0005f * xFactor;
	}
	
	public Projectile Shoot()
	{
		return new Projectile(this.shape.getCenterX(), this.shape.getCenterY(), (float) (180 * Math.atan2(this.shape.getPoint(0)[0]-this.shape.getPoint(2)[0], this.shape.getPoint(0)[1]-this.shape.getPoint(2)[1]) / Math.PI));
	}
}
