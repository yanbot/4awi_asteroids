package at.yanbot.slick.asteroids;

import java.util.List;

import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Transform;

public class Projectile extends Object {

	private long createTime;
	
	public Projectile() {
		super();

		float[] points = { 2.5f, 0, 1.875f, 1.875f, 0, 2.5f, -1.875f, 1.875f, -2.5f, 0, -1.875f, -1.875f, 0, -2.5f,
				1.875f, -1.875f };
		this.shape = new Polygon(points);
	}
	
	public Projectile(float xPosition, float yPosition, float orientation) {
		super();

		float[] points = { 2.5f, 0, 1.875f, 1.875f, 0, 2.5f, -1.875f, 1.875f, -2.5f, 0, -1.875f, -1.875f, 0, -2.5f,
				1.875f, -1.875f };
		this.shape = new Polygon(points);
		
		this.orientation = orientation;
		
        this.xSpeed = 1 * (float)Math.sin(Math.PI * (orientation / 180.0));
        this.ySpeed = 1 * (float)Math.cos(Math.PI * (orientation / 180.0));
        
		Transform temp = Transform.createTranslateTransform(xPosition, yPosition);
		this.shape = this.shape.transform(temp);
		
		this.createTime = System.currentTimeMillis();
	}
	
	public boolean ToDelete(List<Projectile> ls)
	{
		if(System.currentTimeMillis()-this.createTime > 1500) return true;
		else return false;
	}
}
