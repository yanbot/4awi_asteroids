package at.yanbot.slick.asteroids;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Asteroids extends BasicGame {

	Input input;

	Object testobject;
	Ship testship;
	Projectile testprojectile;
	Asteroid testasteroid;

	long startTime;

	List<Projectile> projectiles;
	List<Asteroid> asteroids;

	List<Projectile> removeprojectiles;
	List<Asteroid> removeasteroids;

	Random rand = new Random();

	boolean gameOver = false;

	public Asteroids() {
		super("Asteroids");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		if (gameOver) {
			graphics.setColor(Color.darkGray);
			graphics.setBackground(Color.red);
		}

		graphics.setAntiAlias(true);

		graphics.fill(testship.getShape());

		for (int i = 0; i < projectiles.size(); i++) {
			graphics.fill(projectiles.get(i).getShape());
		}

		for (int i = 0; i < asteroids.size(); i++) {
			graphics.draw(asteroids.get(i).getShape());
		}
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {

		input = gameContainer.getInput();
		testship = new Ship();
		projectiles = new ArrayList<Projectile>();
		asteroids = new ArrayList<Asteroid>();

		removeprojectiles = new ArrayList<Projectile>();
		removeasteroids = new ArrayList<Asteroid>();

		// objects.add(new Object(new Rectangle(100, 100, 100, 100), 0.01f, 0.1f,
		// 0.1f));
		// objects.add(testship);
		// objects.add(new Projectile());
		// objects.add(new Asteroid());

		startTime = 0;

		for (int i = 0; i < 10; i++) {
			int t = rand.nextInt(3);

			if (t == 0) {
				asteroids.add(new Asteroid(0, rand.nextInt(900), (float) rand.nextDouble() * 0.5f,
						((float) rand.nextDouble() - 0.5f), 3));
			} else if (t == 1) {
				asteroids.add(new Asteroid(rand.nextInt(1600), 0, ((float) rand.nextDouble() - 0.5f),
						(float) rand.nextDouble() * 0.5f, 3));
			} else if (t == 2) {
				asteroids.add(new Asteroid(1600, rand.nextInt(900), -(float) rand.nextDouble() * 0.5f,
						((float) rand.nextDouble() - 0.5f), 3));
			} else {
				asteroids.add(new Asteroid(rand.nextInt(1600), 900, ((float) rand.nextDouble() - 0.5f),
						-(float) rand.nextDouble() * 0.5f, 3));
			}

		}
	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {

		if (gameContainer.getInput().isKeyDown(Input.KEY_RIGHT)) {
			testship.setRotation(0.01f);
		} else if (gameContainer.getInput().isKeyDown(Input.KEY_LEFT)) {
			testship.setRotation(-0.01f);
		} else {
			testship.setRotation(0);
		}

		if (gameContainer.getInput().isKeyDown(Input.KEY_UP)) {
			testship.Thrust();
		}

		if (gameContainer.getInput().isKeyDown(Input.KEY_DOWN) && (System.currentTimeMillis() - startTime > 200)) {
			projectiles.add(testship.Shoot());
			startTime = System.currentTimeMillis();
		}

//		testobject.RotateDynamic();
//		testobject.MoveDynamic();
//		testobject.LoopBoundary(1600, 900);
//		testprojectile.MoveDynamic();
//		testasteroid.MoveDynamic();
//		testasteroid.RotateDynamic();
//		testasteroid.LoopBoundary(1600, 900);
//		testship.RotateDynamic();
//		testship.MoveDynamic();
//		testship.LoopBoundary(1600, 900);

		testship.MoveDynamic(delta);
		testship.RotateDynamic(delta);
		testship.LoopBoundary(1600, 900);

		for (Projectile projectile : projectiles) {
			// objects.get(i).RotateDynamic();
			projectile.MoveDynamic(delta);
			projectile.LoopBoundary(1600, 900);
			if (projectile.ToDelete(projectiles)) {
				removeprojectiles.add(projectile);
			}
		}

		for (Asteroid asteroid : asteroids) {
			// objects.get(i).RotateDynamic();
			asteroid.MoveDynamic(delta);
			asteroid.LoopBoundary(1600, 900);

			for (Projectile projectile : projectiles) {
				if (asteroid.shape.intersects(projectile.shape)) {
					removeprojectiles.add(projectile);
					removeasteroids.add(asteroid);
				}
			}
		}

		for (Asteroid asteroid : removeasteroids) {
			if (asteroid.size > 1) {
				asteroids.add(new Asteroid(asteroid.getxPos(), asteroid.getyPos(), ((float) rand.nextDouble() - 0.5f),
						((float) rand.nextDouble() - 0.5f), asteroid.getSize() - 1));
				asteroids.add(new Asteroid(asteroid.getxPos(), asteroid.getyPos(), ((float) rand.nextDouble() - 0.5f),
						((float) rand.nextDouble() - 0.5f), asteroid.getSize() - 1));
			}
			asteroids.remove(asteroid);
		}

		for (Projectile projectile : removeprojectiles) {
			projectiles.remove(projectile);
		}

		removeasteroids.clear();
		removeprojectiles.clear();

		for (Asteroid asteroid : asteroids) {
			if (asteroid.shape.intersects(testship.shape))
				gameOver = false;
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Asteroids());
			container.setDisplayMode(1600, 900, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
