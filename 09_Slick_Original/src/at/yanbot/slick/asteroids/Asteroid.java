package at.yanbot.slick.asteroids;

import java.util.List;

import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Transform;

public class Asteroid extends Object {

	int size;
	
	public Asteroid() {
		float[] points = { 10, 0, 7.5f, 7.5f, 0, 10, -7.5f, 7.5f, -10, 0, -7.5f, -7.5f, 0, -10, 7.5f, -7.5f };
		this.shape = new Polygon(points);
		this.rotation = 0.002f;
		this.xSpeed = 0.2f;
		this.ySpeed = 0.1f;

	}

	public Asteroid(float xPosition, float yPosition, float xSpeed, float ySpeed, int size) {
		float[] points = { 10 * size, 0,
				7.5f * size, 7.5f * size,
				0, 10 * size,
				-7.5f * size, 7.5f * size,
				-10 * size, 0,
				-7.5f * size, -7.5f * size,
				0, -10 * size,
				7.5f  * size, -7.5f * size};
		this.size = size;
		
		this.shape = new Polygon(points);
		this.rotation = 0.002f;
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;

		Transform temp = Transform.createTranslateTransform(xPosition, yPosition);
		this.shape = this.shape.transform(temp);
	}

	public int getSize() {
		return this.size;
	}

}
