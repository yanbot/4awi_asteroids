package at.yanbot.slick.firstgame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.tests.AnimationTest;

public class FirstGame extends BasicGame {
	private double angle;

	private Rectangle rect;
	private Ellipse ellipse;
	private Ellipse circle;

	private boolean bCircleRight;

	private float gameSpeed;

	public FirstGame() {
		super("First Game");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		graphics.draw(rect);

		graphics.draw(ellipse);

		graphics.draw(circle);
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		// TODO Auto-generated method stub
		this.rect = new Rectangle(400, 300, 100, 100);
		this.circle = new Ellipse(gameContainer.getWidth()/2, gameContainer.getHeight()/2, 50, 50);
		this.ellipse = new Ellipse(gameContainer.getWidth()/2, gameContainer.getHeight()/2, 100, 50);
		
		/*
		this.rect.setHeight(100);
		this.rect.setWidth(100);
		this.rect.setX(400);
		this.rect.setY(300);

		this.circle.setRadii(50, 50);
		this.circle.setX(100);
		this.circle.setY(gameContainer.getHeight() / 2 - this.circle.getRadius2());
		this.bCircleRight = true;

		this.ellipse.setRadii(100, 50);
		this.ellipse.setY(100);
		this.ellipse.setX(gameContainer.getWidth() / 2 - this.ellipse.getRadius1());
		*/

		gameSpeed = 0.2f;
	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		// TODO Auto-generated method stub
		if (bCircleRight) {
			this.circle.setX(this.circle.getX() + delta * gameSpeed);
			if (this.circle.getX() > 700)
				bCircleRight = false;
		} else {
			this.circle.setX(this.circle.getX() - delta * gameSpeed);
			if (this.circle.getX() < 0)
				bCircleRight = true;
		}

		this.ellipse.setY(this.ellipse.getY() + delta * gameSpeed);
		if (this.ellipse.getY() + 100 > gameContainer.getHeight())
			this.ellipse.setY(0);

		this.angle += delta * gameSpeed;

		this.rect.setX((float)Math.cos(this.angle * Math.PI / 180) * 150 + gameContainer.getWidth() / 2 - 50);
		this.rect.setY((float)Math.sin(this.angle * Math.PI / 180) * 150 + gameContainer.getHeight() / 2 - 50);
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
